{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f6ba9e27",
   "metadata": {},
   "source": [
    ".. index:: Parallelization\n",
    "\n",
    "# Parallelization\n",
    "\n",
    "Parallelization is available using either distributed memory based on **MPI**\n",
    "or multithreading using **OpenMP**.\n",
    "\n",
    ".. index:: Parallelization; OpenMP\n",
    "\n",
    "## OpenMP\n",
    "\n",
    "It is straightforward to enable some multithreading support. Note that\n",
    "this will speedup assembly and evaluation of the spatial operators but\n",
    "not in general the linear algebra backend so that the overall speedup of\n",
    "the code might not be as high as expected. Since we rely mostly on\n",
    "external packages for the linear algebra, speedup in this step will\n",
    "depend on the multithreading support available in the chosen linear\n",
    "algebra backend - see the discussion on how to\n",
    "[switch between linear solver backends](solversExternal_nb.ipynb)\n",
    "to, for example, use the thread parallel solvers from scipy.\n",
    "\n",
    "By default only a single thread is used. To enable multithreading simply add"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "593e3a2b",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:56:18.985325Z",
     "iopub.status.busy": "2024-12-16T11:56:18.985080Z",
     "iopub.status.idle": "2024-12-16T11:56:19.720794Z",
     "shell.execute_reply": "2024-12-16T11:56:19.719861Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Using 1 threads\n",
      "Using 4 threads\n"
     ]
    }
   ],
   "source": [
    "from dune.fem import threading\n",
    "print(\"Using\",threading.use,\"threads\")\n",
    "threading.use = 4 # use 4 threads\n",
    "print(\"Using\",threading.use,\"threads\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1710c9b1",
   "metadata": {},
   "source": [
    "At startup a maximum number of threads is selected based on the hardware\n",
    "concurrency. This number can be changed by setting the environment\n",
    "variable `DUNE_NUM_THREADS` which sets both the maximum and set the number of\n",
    "threads to use. To get this number or set the number of threads to use to\n",
    "the maximum use"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "71d9723e",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:56:19.725398Z",
     "iopub.status.busy": "2024-12-16T11:56:19.725147Z",
     "iopub.status.idle": "2024-12-16T11:56:19.730249Z",
     "shell.execute_reply": "2024-12-16T11:56:19.729402Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Maximum number of threads available: 32\n",
      "Using 32 threads\n"
     ]
    }
   ],
   "source": [
    "print(\"Maximum number of threads available:\",threading.max)\n",
    "threading.useMax()\n",
    "print(\"Using\",threading.use,\"threads\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "206200e4",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "\n",
    ".. index:: Parallelization; MPI\n",
    "\n",
    "## MPI\n",
    "\n",
    "It is straightforward to use **MPI** for parallelization. It requires a\n",
    "parallel grid, in fact most of the DUNE grids work in parallel except `albertaGrid` or `polyGrid`.\n",
    "Most iterative solvers in DUNE work for parallel runs. Some of the\n",
    "preconditioning methods also work in parallel, a complete list is found at the\n",
    "[bottom of the solver discussion](solversInternal_nb.ipynb).\n",
    "\n",
    "Running a parallel job can be\n",
    "done by using `mpirun`\n",
    "```\n",
    "mpirun -np 4 python script.py\n",
    "```\n",
    "in order to use `4` MPI processes. Example scripts that run in parallel are,\n",
    "for example, the [Re-entrant Corner Problem](laplace-adaptive_nb.ipynb).\n",
    "\n",
    ".. index:  Parallelization; print\n",
    "\n",
    ".. tip:: When running a parallel program output to the console should only be\n",
    "received on dedicated processors, e.g. process 0. This can be achieved by\n",
    "simply overloading the print function as shown below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "e246aaa8",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:56:19.734521Z",
     "iopub.status.busy": "2024-12-16T11:56:19.734343Z",
     "iopub.status.idle": "2024-12-16T11:56:19.737979Z",
     "shell.execute_reply": "2024-12-16T11:56:19.737287Z"
    },
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "from functools import partial\n",
    "from dune.common import comm\n",
    "# print can be used as before but will only produce output on rank 0\n",
    "print = partial(print, flush=True) if comm.rank == 0 else lambda *args, **kwargs: None"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ab5cda5",
   "metadata": {},
   "source": [
    "\n",
    ".. index:  Parallelization; SLURM Batch Script\n",
    "\n",
    "\n",
    ".. tip:: On a cluster where multiple parallel jobs are run simultaneously, it's\n",
    "advisable to use one separate cache per job. This can be easily\n",
    "done by copying an existing cache with pre-compiled modules. See this [SLURM batch script](slurmbatchscript.rst)\n",
    "for an example on how to do this.\n",
    "\n",
    ".. index:: Parallelization; Load Balancing\n",
    "\n",
    "### Load balancing\n",
    "\n",
    "When running distributed memory jobs load balancing is an issue. The specific\n",
    "load balancing method used, depends on the grid implementation.\n",
    "There are two ways to ensure a balanced work load. For computations without\n",
    "dynamic adaptation this only has to be done once in the beginning of the run.\n",
    "\n",
    ".. tip:: Most grid managers will read the grid onto rank zero on\n",
    "   construction. While many grid will then perform a load balance step,\n",
    "   it is good practice to add a call to ``loadbalance`` right after the grid\n",
    "   construction to guarantee the grid is distributed between the\n",
    "   available processes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "85866880",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:56:19.742308Z",
     "iopub.status.busy": "2024-12-16T11:56:19.742128Z",
     "iopub.status.idle": "2024-12-16T11:59:28.950883Z",
     "shell.execute_reply": "2024-12-16T11:59:28.949911Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function aluCubeGrid in module dune.alugrid._grids:\n",
      "\n",
      "aluCubeGrid(*args, **kwargs)\n",
      "    Create an ALUGrid instance.\n",
      "\n",
      "    Note: This functions has to be called on all cores and the parameters passed should be the same.\n",
      "          Otherwise unexpected behavior will occur.\n",
      "\n",
      "    Parameters:\n",
      "    -----------\n",
      "\n",
      "        constructor  means of constructing the grid, i.e. a grid reader or a\n",
      "                     dictionary holding macro grid information\n",
      "        dimgrid      dimension of grid, i.e. 2 or 3\n",
      "        dimworld     dimension of world, i.e. 2 or 3 and >= dimension\n",
      "        comm         MPI communication (not yet implemented)\n",
      "        serial       creates a grid without MPI support (default False)\n",
      "        verbose      adds some verbosity output (default False)\n",
      "        lbMethod     load balancing algorithm. Possible choices are (default is 9):\n",
      "                         0  None\n",
      "                         1  Collect (to rank 0)\n",
      "                         4  ALUGRID_SpaceFillingCurveLinkage (assuming the macro\n",
      "                            elements are ordering along a space filling curve)\n",
      "                         5  ALUGRID_SpaceFillingCurveSerialLinkage (serial version\n",
      "                            of 4 which requires the entire graph to fit to one core)\n",
      "                         9  ALUGRID_SpaceFillingCurve (like 4 without linkage\n",
      "                            storage), this is the default option.\n",
      "                         10 ALUGRID_SpaceFillingCurveSerial (serial version\n",
      "                            of 10 which requires the entire graph to fit to one core)\n",
      "                         11 METIS_PartGraphKway, METIS method PartGraphKway, see\n",
      "                            http://glaros.dtc.umn.edu/gkhome/metis/metis/overview\n",
      "                         12 METIS_PartGraphRecursive, METIS method\n",
      "                            PartGraphRecursive, see\n",
      "                            http://glaros.dtc.umn.edu/gkhome/metis/metis/overview\n",
      "                         13 ZOLTAN_LB_HSFC, Zoltan's geometric load balancing based\n",
      "                            on a Hilbert space filling curve, see https://sandialabs.github.io/Zoltan/\n",
      "                         14 ZOLTAN_LB_GraphPartitioning, Zoltan's load balancing\n",
      "                            method based on graph partitioning, see https://sandialabs.github.io/Zoltan/\n",
      "                         15 ZOLTAN_LB_PARMETIS, using ParMETIS through Zoltan, see\n",
      "                            https://sandialabs.github.io/Zoltan/\n",
      "        lbUnder      value between 0.0 and 1.0 (default 0.0)\n",
      "        lbOver       value between 1.0 and 2.0 (default 1.2)\n",
      "\n",
      "    Returns:\n",
      "    --------\n",
      "\n",
      "    An ALUGrid instance with given refinement (conforming or nonconforming) and element type (simplex or cube).\n",
      "\n"
     ]
    }
   ],
   "source": [
    "from dune.alugrid import aluCubeGrid\n",
    "from dune.grid import cartesianDomain\n",
    "from dune.fem.function import partitionFunction\n",
    "domain = cartesianDomain([0,0], [1,1], [40,40], overlap=1)\n",
    "gridView = aluCubeGrid( domain, lbMethod=9 )\n",
    "\n",
    "# distribute work load, no user data is adjusted\n",
    "gridView.hierarchicalGrid.loadBalance()\n",
    "\n",
    "# print possible load balancing methods\n",
    "help( aluCubeGrid )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "00346a8e",
   "metadata": {},
   "source": [
    "\n",
    "Run this code snippet in parallel and then inspect the different domain decompositions.\n",
    "For plotting of parallel data vtk has to be used. In this case we display the\n",
    "rank information for each partition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "5e2898cd",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:59:28.954452Z",
     "iopub.status.busy": "2024-12-16T11:59:28.954148Z",
     "iopub.status.idle": "2024-12-16T11:59:29.002911Z",
     "shell.execute_reply": "2024-12-16T11:59:29.001853Z"
    }
   },
   "outputs": [],
   "source": [
    "vtk = gridView.sequencedVTK(\"data\", celldata=[partitionFunction(gridView)])\n",
    "vtk()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd048e17",
   "metadata": {},
   "source": [
    "\n",
    "For load balancing with re-distribution of user data the function\n",
    "`dune.fem.loadBalance` should be used. This method is similar to the\n",
    "``dune.fem.adapt`` method discussed in the\n",
    "[section on adaptivity](gridviews.rst#Dynamic-Local-Grid-Refinement-and-Coarsening).\n",
    "See also the [crystal growth](crystal_nb.ipynb) or [Re-entrant Corner Problem](laplace-adaptive_nb.ipynb) examples."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
