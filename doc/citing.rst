###################
Citing this project
###################

If you found this tutorial helpful for getting your own projects up and
running please cite this project:

Title: Python Bindings for the DUNE-FEM module
*Authors: Andreas Dedner, Martin Nolte, and Robert Klöfkorn*
Publisher: Zenodoo, 2020
DOI 10.5281/zenodo.3706994

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3706994.svg
   :target: https://doi.org/10.5281/zenodo.3706994
