get_ipython().run_line_magic('config', "InlineBackend.figure_format = 'jpeg'")
import matplotlib
# matplotlib.rc( 'image', cmap='jet' )
matplotlib.rcParams['figure.dpi'] = 50
matplotlib.rcParams['image.cmap'] = 'jet'
