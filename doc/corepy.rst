.. sectionauthor:: Andreas Dedner <a.s.dedner@warwick.ac.uk>, Martin Nolte <nolte.mrtn@gmail.com>


#############################
Using the Full Grid Interface
#############################

.. toctree::
   :maxdepth: 1

   dune-corepy_nb
